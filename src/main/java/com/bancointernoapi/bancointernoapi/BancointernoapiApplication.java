package com.bancointernoapi.bancointernoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancointernoapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancointernoapiApplication.class, args);
	}

}
