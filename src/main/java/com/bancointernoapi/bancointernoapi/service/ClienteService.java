package com.bancointernoapi.bancointernoapi.service;

import com.bancointernoapi.bancointernoapi.models.Cliente;
import com.bancointernoapi.bancointernoapi.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente buscar(Integer id){

        Optional<Cliente> cliObj = clienteRepository.findById(id);

        return cliObj.orElse(null);
    }

    public Cliente salvar(@PathVariable Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public List<Cliente> buscarClientes(){
        return (List<Cliente>) clienteRepository.findAll();
    }

    public Cliente atualizar(@RequestBody Cliente cliente) {
        return clienteRepository.save(cliente);
    }
    public void deletar(@RequestBody Cliente id) {
        clienteRepository.delete(id);
    }
}
