package com.bancointernoapi.bancointernoapi.resource;

import com.bancointernoapi.bancointernoapi.models.Cliente;
import com.bancointernoapi.bancointernoapi.repository.ClienteRepository;
import com.bancointernoapi.bancointernoapi.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ClienteService clienteService;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> listarCliente(@PathVariable("id") Integer id) {

        Cliente objCli = clienteService.buscar(id);

        return ResponseEntity.ok().body(objCli);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @DeleteMapping("/{id}")
    public void deletaCliente(@RequestBody Cliente id) {
        clienteService.deletar(id);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping("/cliente")
    public Cliente atualizaCliente(@RequestBody Cliente cliente) {
        return clienteService.atualizar(cliente);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/cliente")
    public Cliente salvaCliente(@RequestBody Cliente cliente) {
        return clienteService.salvar(cliente);
    }

    @GetMapping("/")
    public List<Cliente> buscarCliente(){
        return clienteService.buscarClientes();
    }
}

