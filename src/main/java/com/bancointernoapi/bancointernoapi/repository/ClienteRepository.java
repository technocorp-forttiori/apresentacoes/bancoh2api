package com.bancointernoapi.bancointernoapi.repository;

import com.bancointernoapi.bancointernoapi.models.Cliente;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}
